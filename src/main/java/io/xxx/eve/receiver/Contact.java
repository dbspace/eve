package io.xxx.eve.receiver;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Data;

@Data
@Entity
public class Contact implements Receiver {

    @Id
    private Long id;

    private String nickName;

    private String phone;

    @Override
    public Type type() {
        return Type.CONTACT;
    }

    @Override
    public String id() {
        return id.toString();
    }

    @Override
    public String name() {
        return nickName;
    }
}
