package io.xxx.eve.receiver;

public interface Receiver {

    Type type();

    String id();

    String name();

    enum Type {
        CONTACT,

        ROOM
    }
}
