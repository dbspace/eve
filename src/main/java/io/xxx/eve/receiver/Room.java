package io.xxx.eve.receiver;

import io.xxx.eve.seller.Seller;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.Data;

@Data
@Entity
public class Room implements Receiver {

    @Id
    private String id;

    private String topic;

    @ManyToOne
    private Seller seller;

    @Override
    public Type type() {
        return Type.ROOM;
    }

    @Override
    public String id() {
        return id;
    }

    @Override
    public String name() {
        return topic;
    }
}
