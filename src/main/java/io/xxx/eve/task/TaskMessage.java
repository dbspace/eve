package io.xxx.eve.task;

import com.alibaba.fastjson.JSONObject;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

@Data
@Entity
public class TaskMessage {

    @Id
    private Long id;

    private Integer sn;

    @Enumerated
    private Message.Type messageType;

    @Column(columnDefinition = "jsonb")
    @JdbcTypeCode(SqlTypes.JSON)
    private JSONObject payload;

    @ManyToOne
    private Task task;
}
