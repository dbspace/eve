package io.xxx.eve.task;

import jakarta.persistence.Entity;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Entity
public class Task {

    @Id
    private Long id;

    private String name;

    @Enumerated
    private ExecutionMode executionMode;

    @Enumerated
    private SelectMode selectMode;

    @Enumerated
    private SenderType sendMode;

    @OneToMany(mappedBy = "task")
    private List<TaskMessage> messages;

    private LocalDateTime startTime;

    private LocalDateTime endTime;

    private Status status;

    private Integer retryTimes;

    private LocalDateTime createdTime;

    private LocalDateTime updatedTime;

    public enum ExecutionMode {
        ONCE,

        CYCLE
    }

    public enum SelectMode {
        DATABASE,

        XLSX
    }

    public enum SenderType {
        IPAD,
        PHONE
    }

    public enum Status {
        NEW,
        RUNNING,
        CANCELED,
        COMPLETED
    }
}
