package io.xxx.eve.task;

import com.alibaba.fastjson.JSONObject;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Data;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

@Data
@Entity
public class Message {

    @Id
    private Long id;

    private Type type;

    @Column(columnDefinition = "jsonb")
    @JdbcTypeCode(SqlTypes.JSON)
    private JSONObject payload;

    public enum Type {

        TEXT,
        IMAGE,
        VIDEO,
        MINI_PROGRAM
    }
}
