package io.xxx.eve.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "imadcn")
public class ImadcnProperties {

    private Zookeeper zookeeper;

    @Data
    public static class Zookeeper {

        private String servers;
    }
}
