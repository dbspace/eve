package io.xxx.eve.config;

import com.imadcn.framework.idworker.config.ApplicationConfiguration;
import com.imadcn.framework.idworker.config.ZookeeperConfiguration;
import com.imadcn.framework.idworker.generator.IdGenerator;
import com.imadcn.framework.idworker.generator.SnowflakeGenerator;
import com.imadcn.framework.idworker.register.zookeeper.ZookeeperWorkerRegister;
import com.imadcn.framework.idworker.registry.zookeeper.ZookeeperRegistryCenter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(ImadcnProperties.class)
public class IdWorkerConfiguration {

    @Autowired
    private ImadcnProperties properties;

    @Bean
    public IdGenerator idGenerator() {
        ZookeeperConfiguration zookeeperConfiguration = new ZookeeperConfiguration();
        zookeeperConfiguration.setServerLists(properties.getZookeeper().getServers());
        ZookeeperRegistryCenter center = new ZookeeperRegistryCenter(zookeeperConfiguration);
        ZookeeperWorkerRegister register = new ZookeeperWorkerRegister(center, new ApplicationConfiguration());
        return new SnowflakeGenerator(register);
    }
}
