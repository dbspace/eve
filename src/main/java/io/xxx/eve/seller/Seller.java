package io.xxx.eve.seller;

import io.xxx.eve.robot.Robot;
import io.xxx.eve.receiver.Room;
import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Data
@Entity
public class Seller {

    @Id
    private Long id;

    @Column(length = 30)
    private String wxId;

    @Column(length = 64)
    private String name;

    @Column(length = 20)
    private String phone;

    @OneToMany(mappedBy = "seller")
    private List<Room> rooms;

    @ManyToMany
    private List<Robot> robots;
}
