package io.xxx.eve.robot.send;

import io.xxx.eve.robot.Talker;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * 每个机器人推送限制
 */
@Order(1)
@Component
public class AloneTalkLimiter implements Talker.TalkLimiter {

    @Override
    public boolean tryAcquire() {
        return true;
    }
}
