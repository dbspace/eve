package io.xxx.eve.robot.send;

import io.xxx.eve.robot.Talker;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * 全局机器人推送限制
 */
@Order(2)
@Component
public class GlobalTalkLimiter implements Talker.TalkLimiter {

    @Override
    public boolean tryAcquire() {
        return true;
    }
}
