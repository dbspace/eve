package io.xxx.eve.robot.send;

import com.alibaba.fastjson.JSONObject;
import io.xxx.eve.receiver.Receiver;
import io.xxx.eve.robot.Talker;
import io.xxx.eve.robot.device.ServerHandler;
import io.xxx.eve.task.TaskMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 通过长连接向手机端的APP发送待推送的消息，手机端收到消息后执行推送，并返回推送结果。
 */
@Component
public class PhoneSender implements Talker.Sender {

    @Autowired
    private ServerHandler handler;

    @Override
    public JSONObject say(Receiver receiver, TaskMessage message) {
        return handler.sendMessage(receiver.id(), message);
    }
}
