package io.xxx.eve.robot;

import io.xxx.eve.robot.send.IPadSender;
import io.xxx.eve.robot.send.PhoneSender;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RRateLimiter;
import org.redisson.api.RedissonClient;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class TalkerFactory implements ApplicationRunner, ApplicationContextAware {

    @Autowired
    private RobotRepository robotRepository;

    @Autowired
    private GenericApplicationContext applicationContext;

    @Autowired
    private RedissonClient redissonClient;

    @Autowired
    private List<Talker.TalkLimiter> limiters;

    @Autowired
    private IPadSender iPadSender;

    @Autowired
    private PhoneSender phoneSender;

    public Talker getTalker(Robot robot) {
        return applicationContext.getBean("talker_" + robot.getId(), Talker.class);
    }

    @Override
    public synchronized void run(ApplicationArguments args) {
        log.info("机器人启动开始");
        List<Robot> robots = robotRepository.findAll();
        for (Robot robot : robots) {
            RRateLimiter rateLimiter = redissonClient.getRateLimiter("robot_rateLimiter_" + robot.getId());
            BeanDefinition beanDefinition = BeanDefinitionBuilder.rootBeanDefinition(Talker.class)
                    .addConstructorArgValue(rateLimiter)
                    .addConstructorArgValue(limiters)
                    .addConstructorArgValue(iPadSender)
                    .addConstructorArgValue(phoneSender)
                    .getBeanDefinition();
            String beanName = "talker_" + robot.getId();
            applicationContext.registerBeanDefinition(beanName, beanDefinition);
            Talker talker = applicationContext.getBean(beanName, Talker.class);
            Thread.startVirtualThread(talker);
        }
        log.info("机器人启动完毕");
    }

    @Override
    public void setApplicationContext(@NonNull ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = (GenericApplicationContext) applicationContext;
    }
}
