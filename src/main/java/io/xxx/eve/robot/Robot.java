package io.xxx.eve.robot;

import io.xxx.eve.seller.Seller;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import lombok.Data;

import java.util.List;

@Data
@Entity
public class Robot {

    @Id
    private Long id;

    private String wxId;

    @ManyToMany(mappedBy = "robot")
    private List<Seller> sellers;
}
