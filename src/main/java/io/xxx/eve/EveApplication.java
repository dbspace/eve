package io.xxx.eve;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class EveApplication {

    public static void main(String[] args) {
        SpringApplication.run(EveApplication.class, args);
    }

}
